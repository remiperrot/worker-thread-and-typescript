import { Worker } from 'worker_threads';
import { sleep } from './timeout';

setInterval(() => process.stdout.write('.'), 500);

const instantiatedWorker = new Worker('./src/worker.js', {
    workerData: {
        path: './worker.ts',
        value: 120,
    },
});

instantiatedWorker.postMessage({
    type: 'actions',
    value: { name: 'My event', id: 'An ID' },
});

sleep(2000, () => {
    console.log('[Main] - Sending new msg');
    instantiatedWorker.postMessage({
        type: 'vtt',
        value: { name: 'My second event', id: 'The ID' },
    });
});
