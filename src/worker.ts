import { parentPort, workerData, isMainThread } from 'worker_threads';

setInterval(() => process.stdout.write('·'), 500);
function writeTime() {
    const d = new Date();
    console.log(`[Worker] ${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`);
}

// https://www.jesuisundev.com/comprendre-les-worker-threads-de-nodejs/
function intensiveCpuTaskIo(type?: string) {
    console.log(`[Worker] Launching intensive CPU task - ${type}`);
    writeTime();
    let increment = 0;
    while (increment !== Math.pow(10, 10)) {
        increment++;
    }
    console.log(
        '[Worker] Intensive CPU task is done ! Result is : ',
        increment
    );
    writeTime();
}

parentPort?.on('message', (message) => {
    console.log('message', message);
    intensiveCpuTaskIo(message.type);
});
