export function timeout(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}

export async function sleep(sleepTimeout: number, fn: any, ...args: any) {
    await timeout(sleepTimeout);
    return fn(...args);
}
