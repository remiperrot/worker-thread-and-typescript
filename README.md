# Testing node worker with Typescript

## Initialization

1. Install `yarn`
2. Inside this repository, launch the following command: `yarn`.

## The POC

Everything is in `/src`. `main.ts` is the initializing file. It calls a `worker.js` (because for some compilation reason it's not possible to have a `ts` file with `ts-node`), which calls the `worker.ts` - being the message handler in our POC.

`main.ts` regularly writes `.` to the console. `worker.ts` writes `·`.

Launching everything with `yarn start`, you'll have some output like:

```bash
❯ yarn start                                                                 09:15:39
yarn run v1.22.18
warning ../package.json: No license field
$ ts-node src/main.ts
..message { type: 'actions', value: { name: 'My event', id: 'An ID' } }
..[Main] - Sending new msg
..................................[Worker] Launching intensive CPU task - actions
[Worker] 9:20:32
[Worker] Intensive CPU task is done ! Result is :  10000000000
[Worker] 9:20:41
message { type: 'vtt', value: { name: 'My second event', id: 'The ID' } }
[Worker] Launching intensive CPU task - vtt
[Worker] 9:20:41
[Worker] Intensive CPU task is done ! Result is :  10000000000
[Worker] 9:20:50
·.·.·.·.·.·
```

Which shows that:

-   Main execution keeps happening correctly (lots of `.`)
-   Worker execution is blocked by an intensive task
-   It does the tasks one by one
-   When all tasks are done, both worker & main thread are available

Don't hesitate to modify the file and message sending to try this.

## Sources

-   [Comprendre les Worker Threads de NodeJS - Je suis un dev](https://www.jesuisundev.com/comprendre-les-worker-threads-de-nodejs/)
-   [Simple bidirectional messaging in Node.js Worker Threads | by Jeff Lowery | Level Up Coding](https://levelup.gitconnected.com/simple-bidirectional-messaging-in-node-js-worker-threads-7fe41de22e3c)
-   [Worker threads | Node.js v17.8.0 Documentation](https://nodejs.org/api/worker_threads.html)
-   [Worker Threads with TypeScript - Node.js TypeScript #12](https://wanago.io/2019/05/06/node-js-typescript-12-worker-threads/)
-   [Node.js TypeScript #13. Sending data between Worker Threads](https://wanago.io/2019/05/13/node-js-typescript-13-sending-data-worker-threads/)
